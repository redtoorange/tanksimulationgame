// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "TankPlayerController.generated.h"

class UTankAimingComponent;
class ATank;

/**
 * 
 */
UCLASS()
class TANKSIMULATIONGAME_API ATankPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	void BeginPlay() override;
	void Tick(float DeltaTime) override;


	UFUNCTION(BlueprintCallable, Category="Tank")
	ATank* GetControlledTank() const;

	UFUNCTION(BlueprintImplementableEvent)
	void FoundAimingComponent(UTankAimingComponent* aimingComponentRef);

private:
	UTankAimingComponent* TankAimingComponent = nullptr;

	bool GetSightRayHitLocation(FVector& hitLocation) const;

	FVector GetStartAndEnd(FVector& start, FVector& end) const;

	virtual void AimTowardsCrosshair();

	ATank* ControlledTank = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = "Interface")
	float CrossHairX = 0.5f;

	UPROPERTY(EditDefaultsOnly, Category = "Interface")
	float CrossHairY = 1.0/3.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Interface")
	float CastRange = 1000000.0f;
};
