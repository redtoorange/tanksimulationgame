// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Pawn.h"
#include "Tank.generated.h"

UCLASS()
class TANKSIMULATIONGAME_API ATank : public APawn
{
	GENERATED_BODY()

public:
	ATank();

	void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
};
