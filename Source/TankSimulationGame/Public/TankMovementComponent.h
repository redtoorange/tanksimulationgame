// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/NavMovementComponent.h"
#include "TankMovementComponent.generated.h"

class UTankTrack;

/**
 * 
 */
UCLASS(meta = (BlueprintSpawnableComponent) )
class TANKSIMULATIONGAME_API UTankMovementComponent : public UNavMovementComponent
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, Category="Tank")
	void IntendMoveForward( float Throw );

	UFUNCTION(BlueprintCallable, Category = "Tank")
	void IntendMoveClockwise(float Throw);
	
	UFUNCTION(BlueprintCallable, Category="Tank")
	void Initialize(UTankTrack* LeftTrackToSet, UTankTrack* RightTrackToSet);

	virtual void RequestDirectMove(	const FVector& MoveVelocity, bool bForceMaxSpeed) override;

private:
	UTankTrack* LeftTrack = nullptr;
	UTankTrack* RightTrack = nullptr;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Tank")
	float speed = 10.0f;



};
