// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/StaticMeshComponent.h"
#include "TankBarrel.generated.h"


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TANKSIMULATIONGAME_API UTankBarrel : public UStaticMeshComponent
{
	GENERATED_BODY()
	
public:
	void Elevate( float relativeSpeed );
	
private:
	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	float MaxDegreesPerSecond = 20.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	float MaxElevation = 30.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	float MinElevation = -0.8f;
};
