// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "TankAimingComponent.generated.h"

class UTankBarrel;
class UTankTurret;
class AProjectile;

UENUM(BlueprintType)
enum class EAimingState : uint8
{
	RELOADING, AIMING, LOCKED
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TANKSIMULATIONGAME_API UTankAimingComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTankAimingComponent();

	void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	
	void BeginPlay() override;

	void AimAt(const FVector& newTarget);

	FVector GetTargetLocation() const;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Firing")
	EAimingState CurrentState = EAimingState::LOCKED;

	UFUNCTION(BlueprintCallable, Category = "Tank")
	void Initialize( UTankBarrel* barrel, UTankTurret* turret);

	UFUNCTION(BlueprintCallable, Category = "Firing")
	void Fire();

	UFUNCTION(BlueprintCallable, Category = "Firing")
	bool AllowedToFire() const;
	
private:
	void MoveBarrel( const FVector& rot );

	float RemainingReloadTime = 0.0f;

	bool CanFire = true;

	FVector targetLocation;

	UTankBarrel* TankBarrel = nullptr;

	UTankTurret* TankTurret = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = "Firing")
	float LaunchSpeed = 100000.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Firing")
	float ElevationSpeed = 10.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Firing")
	float ReloadTime = 3.0f;

	UPROPERTY(EditAnywhere, Category = "Firing")
	bool m_allowedToFire = true;

	UPROPERTY(EditDefaultsOnly, Category = "Firing")
	TSubclassOf<AProjectile> ProjectileBlueprint;
	
};
