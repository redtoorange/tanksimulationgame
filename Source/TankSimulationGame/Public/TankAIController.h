// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "TankAIController.generated.h"

class ATank;
class UTankAimingComponent;

/**
 * 
 */
UCLASS()
class TANKSIMULATIONGAME_API ATankAIController : public AAIController
{
	GENERATED_BODY()
	
public:
	void BeginPlay() override;

	void Tick(float DeltaTime) override;
	
	ATank* GetControlledTank() const;
	
	ATank* GetPlayerTank() const;

	UPROPERTY(VisibleAnywhere, Category = "Controller")
	ATank* PlayerTank = nullptr;

	UPROPERTY(VisibleAnywhere, Category = "Controller")
	ATank* ControlledTank = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Pathfinding")
	float AcceptanceRadius = 3000.0f;

private:
	UTankAimingComponent* TankAimingComponent = nullptr;

};
