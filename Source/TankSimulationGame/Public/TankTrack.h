// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/StaticMeshComponent.h"
#include "TankTrack.generated.h"

/**
 * 
 */
UCLASS(meta = (BlueprintSpawnableComponent) )
class TANKSIMULATIONGAME_API UTankTrack : public UStaticMeshComponent
{
	GENERATED_BODY()
	

public:
	UFUNCTION(BlueprintCallable, Category = "Input")
	void SetThrottle(float amount);
	
	
private:
	UPROPERTY( VisibleAnywhere, Category="Driving")
	float throttle = 0.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Driving")
	float maxForward = 1.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Driving")
	float maxReverse = -1.0f;

	//Max force per track, in newtons.
	UPROPERTY(EditDefaultsOnly, Category = "Driving")
	float maxDrivingForce = 400000.f;
};
