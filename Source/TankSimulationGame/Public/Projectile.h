// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Projectile.generated.h"

UCLASS()
class TANKSIMULATIONGAME_API AProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	AProjectile();

	virtual void Tick(float DeltaTime) override;

	void LaunchProjectile(float);

	virtual void BeginPlay() override;
	

private:
	UProjectileMovementComponent* projectileMovementComponent = nullptr;
};
