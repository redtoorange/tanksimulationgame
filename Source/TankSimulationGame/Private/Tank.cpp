// Fill out your copyright notice in the Description page of Project Settings.

#include "TankSimulationGame.h"
#include "Public/Tank.h"


// Sets default values
ATank::ATank()
{
	UE_LOG(LogTemp, Warning, TEXT("TANK_H: C++ Construction"));
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	
	PrimaryActorTick.bCanEverTick = true;
}


void ATank::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}
