// Fill out your copyright notice in the Description page of Project Settings.

#include "TankSimulationGame.h"
#include "TankBarrel.h"


/**
 *	Elevate the tank's barrel by a percentage of it's barrel elevation change speed.
 *	\param relativeSpeed 0.0 - 1.0 percentage of the barrel's maximum speed.
 */
void UTankBarrel::Elevate(float relativeSpeed)
{
	relativeSpeed = FMath::Clamp<float>(relativeSpeed, -1.0f, 1.0f);
	float elevationChange = relativeSpeed * MaxDegreesPerSecond * GetWorld()->DeltaTimeSeconds;
	float rawElevation = RelativeRotation.Pitch + elevationChange;
	float clampedElevation = FMath::Clamp<float>(rawElevation, MinElevation, MaxElevation);

	SetRelativeRotation(FRotator(clampedElevation, 0, 0));
}