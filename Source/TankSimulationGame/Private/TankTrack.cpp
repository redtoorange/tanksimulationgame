// Fill out your copyright notice in the Description page of Project Settings.

#include "TankSimulationGame.h"
#include "TankTrack.h"


void UTankTrack::SetThrottle(float amount)
{
	throttle = amount;
	throttle = FMath::Clamp(throttle, maxReverse, maxForward);

	FVector forceApplied = GetForwardVector() * throttle * maxDrivingForce;
	FVector forceLocation = GetComponentLocation();

	UPrimitiveComponent* TankRoot = Cast<UPrimitiveComponent>(GetOwner()->GetRootComponent());
	TankRoot->AddForceAtLocation(forceApplied, forceLocation);
}

////i am computer
////watch me compute
////dot dot dash dot ping zero
////program
////program
//i
//am
//robot
//if (robot);
//then (robot)
//else (nobot)
