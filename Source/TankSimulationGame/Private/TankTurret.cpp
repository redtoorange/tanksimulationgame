// Fill out your copyright notice in the Description page of Project Settings.

#include "TankSimulationGame.h"
#include "TankTurret.h"




void UTankTurret::Rotate(float RelativeRotationSpeed)
{
	RelativeRotationSpeed = FMath::Clamp(RelativeRotationSpeed, -1.0f, 1.0f);
	float rotation = (RelativeRotationSpeed * MaxDegreesPerSecond * GetWorld()->DeltaTimeSeconds) + RelativeRotation.Yaw;

	SetRelativeRotation(FRotator(0, rotation, 0));
}