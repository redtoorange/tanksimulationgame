// Fill out your copyright notice in the Description page of Project Settings.

#include "TankSimulationGame.h"
#include "TankMovementComponent.h"
#include "TankTrack.h"



void UTankMovementComponent::IntendMoveForward(float Throw)
{
	if (!LeftTrack || !RightTrack ) return;

	LeftTrack->SetThrottle(Throw);
	RightTrack->SetThrottle(Throw);
}

void UTankMovementComponent::IntendMoveClockwise(float Throw)
{
	if (!LeftTrack || !RightTrack) return;

	LeftTrack->SetThrottle(Throw);
	RightTrack->SetThrottle(-Throw);
}

void UTankMovementComponent::Initialize(UTankTrack* LeftTrackToSet, UTankTrack* RightTrackToSet)
{
	if (LeftTrackToSet && RightTrackToSet)
	{
		LeftTrack = LeftTrackToSet;
		RightTrack = RightTrackToSet;
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Failed to initialize track, references are null."));
	}
}

void UTankMovementComponent::RequestDirectMove(const FVector& MoveVelocity, bool bForceMaxSpeed)
{
	//Eat the request, manipulate the tank's movement
	FVector norm = MoveVelocity.GetSafeNormal();
	FVector forward = GetOwner()->GetActorForwardVector();

	IntendMoveForward( FVector::DotProduct(forward, norm) );
//	UE_LOG(LogTemp, Warning, TEXT("%s vectoring to : %s"), *GetOwner()->GetName(), *norm.ToString());
	IntendMoveClockwise(FVector::CrossProduct(forward, norm).Z);
}
