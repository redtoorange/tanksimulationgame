// Fill out your copyright notice in the Description page of Project Settings.

#include "TankSimulationGame.h"
#include "Public/TankAimingComponent.h"
#include "TankBarrel.h"
#include "TankTurret.h"
#include "Public/Projectile.h"

UTankAimingComponent::UTankAimingComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}


void UTankAimingComponent::BeginPlay()
{
	Super::BeginPlay();
}


void UTankAimingComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (RemainingReloadTime > 0)
	{
		RemainingReloadTime -= DeltaTime;
		if (RemainingReloadTime <= 0)
			CanFire = true;
	}
}


/**
 *	Calculate a new firing arc to hit the target, then move the barrel to the correct position.
 * \param newTarget		The target position in world space to aim at.
 * \param LaunchSpeed	The speed that the projectile will be fired at.
 */
void UTankAimingComponent::AimAt(const FVector& newTarget )
{
	targetLocation = newTarget;

	//Calculate firing solution
	FVector launch(0);
	bool success = UGameplayStatics::SuggestProjectileVelocity(
		this,	//World Object (Usually this)
		launch,	//the calculated velocity
		TankBarrel->GetSocketLocation("Muzzle"), // Start Pos
		targetLocation,	//	End Pos
		LaunchSpeed,	//	Speed of projectile
		false,			//	High arc?
		0,				//	Collision Radius
		0,				//	Gravity Override?
		ESuggestProjVelocityTraceOption::DoNotTrace	// Trace along the route?
	);

	//Move the Barrel
	if( success )
		MoveBarrel( launch );
}


FVector UTankAimingComponent::GetTargetLocation() const
{
	return targetLocation;
}

/**
 *	Move the barrel to the correct elevation.
 *	\param lengthVector The length vector of the required barrel position.  Calculated
 *						by the suggested projectile velocity function.
 */
void  UTankAimingComponent::MoveBarrel(const FVector& lengthVector)
{
	//Convert firing solution to a unit vector then to a rotation
	FRotator rotation = lengthVector.GetSafeNormal().Rotation();

	FRotator barrelRotation = TankBarrel->GetComponentRotation();
	FRotator turretRotation = TankTurret->GetComponentRotation();

	//Find the delta elevation change and move the barrel
	float deltaElevation = (rotation.Pitch - barrelRotation.Pitch);
	float deltaRotation = rotation.Yaw - turretRotation.Yaw;

	TankBarrel->Elevate(deltaElevation);
	TankTurret->Rotate(deltaRotation);	
}


void UTankAimingComponent::Initialize(UTankBarrel* barrel, UTankTurret* turret)
{
	if( ensure(barrel) )
		TankBarrel = barrel;

	if( ensure(turret) )
		TankTurret = turret;
}

void UTankAimingComponent::Fire()
{
	if (CanFire && ensure(ProjectileBlueprint && TankBarrel) )
	{
		CanFire = false;
		RemainingReloadTime = ReloadTime;

		FVector pos = TankBarrel->GetSocketLocation(FName("Muzzle"));
		FRotator rot = TankBarrel->GetSocketRotation(FName("Muzzle"));

		AProjectile* proj = GetWorld()->SpawnActor<AProjectile>(ProjectileBlueprint, pos, rot);
		proj->LaunchProjectile(LaunchSpeed);
	}
}


bool UTankAimingComponent::AllowedToFire() const
{
	return m_allowedToFire;
}