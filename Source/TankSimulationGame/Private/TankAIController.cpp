// Fill out your copyright notice in the Description page of Project Settings.

#include "TankSimulationGame.h"
#include "Public/TankAIController.h"
#include "Public/Tank.h"
#include "TankAimingComponent.h"
#include "TankPlayerController.h"



/**
 * \return The ATank this AI controls.
 */
ATank* ATankAIController::GetControlledTank() const
{
	return Cast<ATank>(GetPawn());
}


/**
 * Cache references to this controller's and the player's ATank.
 */
void ATankAIController::BeginPlay()
{
	Super::BeginPlay();

	PlayerTank = GetPlayerTank();
	ControlledTank = GetControlledTank();

	TankAimingComponent = ControlledTank->FindComponentByClass<UTankAimingComponent>();
	ensure(TankAimingComponent);
}


/**
 * \return The player's ATank.
 */
ATank* ATankAIController::GetPlayerTank() const
{
	ATankPlayerController* pc = Cast<ATankPlayerController>( GetWorld()->GetFirstPlayerController() );
	if(!pc)
	{
		UE_LOG(LogTemp, Error, TEXT("No TankPlayerController found."));
		return nullptr;
	}


	ATank* playerTank = pc->GetControlledTank();
	if (!playerTank)
	{
		UE_LOG(LogTemp, Error, TEXT("No PlayerTank found."));
	}
	
	return playerTank;
}

/**
 *	Get the Player's ATank position and aim towards it.
 * \param DeltaTime Time since last update.
 */
void ATankAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if( ensure(ControlledTank && PlayerTank && TankAimingComponent) )
	{
		TankAimingComponent->AimAt(PlayerTank->GetActorLocation());

		if(TankAimingComponent->AllowedToFire())
			TankAimingComponent->Fire();

		MoveToActor(PlayerTank, AcceptanceRadius);
	}
		
}