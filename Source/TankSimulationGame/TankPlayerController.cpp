// Fill out your copyright notice in the Description page of Project Settings.

#include "TankSimulationGame.h"
#include "TankPlayerController.h"
#include "TankAimingComponent.h"
#include "Public/Tank.h"

/**
 *	Returns the currently controlled Tank by this player controller.
 *	\return 
 */
ATank* ATankPlayerController::GetControlledTank() const
{
	return Cast<ATank>(GetPawn());
}


/**
 *	Called at the beginning of play, everything should already be constructed.
 */
void ATankPlayerController::BeginPlay()
{
	ControlledTank = GetControlledTank();
	TankAimingComponent = ControlledTank->FindComponentByClass<UTankAimingComponent>();
	
	if ( ensure(TankAimingComponent) )
		FoundAimingComponent(TankAimingComponent);

	Super::BeginPlay();
}


void ATankPlayerController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	AimTowardsCrosshair();
}


/**
 *	Move the tank's barrel and turret to face the rotation and elevation required to hit the target
 *	that the crosshair is pointing at.
 */
void ATankPlayerController::AimTowardsCrosshair()
{
	ensure(ControlledTank && TankAimingComponent);

	FVector hitLocation;
	if (GetSightRayHitLocation(hitLocation))
		TankAimingComponent->AimAt(hitLocation);
}

/**
 *	Ray Trace up to CastRange and get the first blocking hit.
 *	\param hitLocation	The location of the impact on the target mesh
 *	\return True if something was hit
 */
bool ATankPlayerController::GetSightRayHitLocation(FVector& hitLocation) const
{
	bool success = false;

	FVector start;
	FVector end;
	FHitResult hitResult;

	GetStartAndEnd(start, end);
	GetWorld()->LineTraceSingleByChannel(hitResult, start, end, ECC_Visibility, FCollisionQueryParams(FName(""), false, ControlledTank));
	
	if (hitResult.GetActor())
	{
		success = true;
		hitLocation = hitResult.Location;
	}

	return success;
}

/**
 * Generate the start and end point for a ray trace based on the camera's rotation and position.
 * \param out_start the start point, the camera's location.
 * \param out_end the ending point based on the camera's rotation and the cast range.
 * \return unit vector of the direction that the camera is pointing.
 */
FVector ATankPlayerController::GetStartAndEnd(FVector& out_start, FVector& out_end) const
{
	int32 x;
	int32 y;
	GetViewportSize(x, y);

	FVector worldDirection;
	FVector loc;
	DeprojectScreenPositionToWorld(x * CrossHairX, y * CrossHairY, loc, worldDirection);
	
	out_start = PlayerCameraManager->GetCameraLocation();
	out_end = out_start + (worldDirection * CastRange);

	return worldDirection;
}


